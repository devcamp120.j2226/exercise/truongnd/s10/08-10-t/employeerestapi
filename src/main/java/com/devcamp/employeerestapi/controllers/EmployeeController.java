package com.devcamp.employeerestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    @CrossOrigin
    @GetMapping("/employees")
    public ArrayList<Employee> getListEmployeeItem(){
        Employee nhanvien1 = new Employee(1,"Nguyen","Huy",10000);
        Employee nhanvien2 = new Employee(2,"Tran","Tuong",20000);
        Employee nhanvien3 = new Employee(3,"Do","Phat",30000);

        System.out.println(nhanvien1.toString());
        System.out.println(nhanvien2.toString());
        System.out.println(nhanvien3.toString());

        ArrayList<Employee> employeeItems = new ArrayList<>();
        employeeItems.add(nhanvien1);
        employeeItems.add(nhanvien2);
        employeeItems.add(nhanvien3);

        return employeeItems;
    }
}
